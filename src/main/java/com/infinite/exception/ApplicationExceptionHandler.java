package com.infinite.exception;


import com.infinite.dto.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler({UserAlreadyExists.class, UserNotFound.class})
    public ResponseEntity<ApiResponse> handleBadCredentials(RuntimeException exception){

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage(exception.getLocalizedMessage());
        apiResponse.setDateTime(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(apiResponse);

    }


}
