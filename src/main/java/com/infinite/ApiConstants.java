package com.infinite;

public enum ApiConstants {


    USER_ALREADY_EXISTS("User Already Exists with the EmailId $s"),
    SIGN_UP_SUCCESSFULLY("Sign Up Successfully"),
    USER_NOT_FOUND("USER NOT FOUND");

    private String constant;

    private ApiConstants(String s) {
        this.constant = s;
    }

    @Override
    public String toString() {
        return constant;
    }
}
