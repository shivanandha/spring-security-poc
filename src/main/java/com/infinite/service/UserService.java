package com.infinite.service;

import com.infinite.dto.ApiResponse;
import com.infinite.dto.SignInRequest;
import com.infinite.dto.SignUpRequest;

public interface UserService {


    ApiResponse signUp(SignUpRequest request);

    ApiResponse signIn(SignInRequest request);
}
