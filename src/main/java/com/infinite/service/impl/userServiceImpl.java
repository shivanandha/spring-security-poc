package com.infinite.service.impl;

import com.infinite.dto.ApiResponse;
import com.infinite.dto.SignInRequest;
import com.infinite.dto.SignUpRequest;
import com.infinite.entity.User;
import com.infinite.exception.UserAlreadyExists;
import com.infinite.exception.UserNotFound;
import com.infinite.repo.UserRepository;
import com.infinite.security.JwtProvider;
import com.infinite.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.infinite.ApiConstants.SIGN_UP_SUCCESSFULLY;
import static com.infinite.ApiConstants.USER_ALREADY_EXISTS;
import static com.infinite.ApiConstants.USER_NOT_FOUND;
import static java.time.LocalDateTime.now;

@Service
public class userServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
   private JwtProvider jwtProvider;


    @Override
    public ApiResponse signUp(SignUpRequest request) {
        userRepository.findByEmail(request.getEmail()).ifPresent(email -> {
            throw new UserAlreadyExists(String.format(String.valueOf(USER_ALREADY_EXISTS), email));
        });

        User user = new User();
        BeanUtils.copyProperties(request, user );

        userRepository.save( user );

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setDateTime(now());
        apiResponse.setMessage(String.valueOf(SIGN_UP_SUCCESSFULLY));
        return apiResponse;


    }

    @Override
    public ApiResponse signIn(SignInRequest request) {
        Map<String, String> token = new HashMap<>();

        User user = userRepository.findByEmailAndPassword(request.getEmail(), request.getPassword())
                .orElseThrow(() -> new UserNotFound(String.valueOf(USER_NOT_FOUND)));

        String userToken = jwtProvider.generateToken(user.getId());

        token.put("authToken", userToken);

        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData((Optional.of(token)));
        apiResponse.setMessage(String.valueOf(SIGN_UP_SUCCESSFULLY));
        return apiResponse;

    }
}
