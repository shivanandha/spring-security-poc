package com.infinite.dto;

import java.time.LocalDateTime;
import java.util.Optional;


public class ApiResponse {

   private String message;
   private LocalDateTime dateTime;
   private Optional data;

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public LocalDateTime getDateTime() {
      return dateTime;
   }

   public void setDateTime(LocalDateTime dateTime) {
      this.dateTime = dateTime;
   }

   public Optional getData() {
      return data;
   }

   public void setData(Optional data) {
      this.data = data;
   }
}
