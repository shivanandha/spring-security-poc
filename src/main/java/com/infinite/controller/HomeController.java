package com.infinite.controller;

import com.infinite.dto.ApiResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/home")
public class HomeController {


    @GetMapping
    public ResponseEntity<ApiResponse> testAccess(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMessage("home controller");
        apiResponse.setDateTime(LocalDateTime.now());
        return ResponseEntity.ok().body(apiResponse);
    }
}
