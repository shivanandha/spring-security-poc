package com.infinite.controller;

import com.infinite.dto.ApiResponse;
import com.infinite.dto.SignInRequest;
import com.infinite.dto.SignUpRequest;
import com.infinite.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private UserService customerService;



    @PostMapping
    public ResponseEntity<ApiResponse> signUp(
            @Valid @RequestBody SignUpRequest request, HttpServletRequest httpServletRequest){
        return ResponseEntity.ok().body(customerService.signUp(request));
    }

    @PostMapping("/auth")
    public ResponseEntity<ApiResponse> signIn(@Valid @RequestBody SignInRequest request){
        return ResponseEntity.ok().body(customerService.signIn(request));
    }
}
