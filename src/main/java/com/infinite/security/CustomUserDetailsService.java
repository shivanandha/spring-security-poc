package com.infinite.security;

import com.infinite.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;



    @Transactional
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {


       return userRepository
                .findByEmail(email)
                .map(UserPrincipal::from)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", email)));




    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        return userRepository
                .findById(id)
                .map(UserPrincipal::from)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User %s not found", id)));

    }
}
